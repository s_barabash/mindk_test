import { Controller, Get, HttpCode } from "@nestjs/common";

@Controller()
export class AppController {
  @Get("/health")
  @HttpCode(200)
  healthCheck(): string {
    return "OK from local src11113333322222_____";
  }
}
