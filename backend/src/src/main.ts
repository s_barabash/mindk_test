import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import * as bodyParser from 'body-parser';

import { AppModule } from '@app/app/app.module';

global['fetch'] = require('node-fetch');

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bodyParser: false,
  });

  app.enableCors({
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    credentials: true,
  });



  app.use(
    bodyParser.json({
      verify: (req, res, buf) => {
        // @ts-ignore
        req.rawBody = buf;
      },
    }),
  );

  const port = process.env.PORT ?? 5555;
  await app.listen(port, () => Logger.log(`🚀 Test ready at port ${port}`));
}
bootstrap();
