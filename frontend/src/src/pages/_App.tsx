import { BrowserRouter, Switch, Route } from "react-router-dom";

import { Home } from "./Home";
import { Health } from "./Health";

export const App = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" render={() => <Home />} />
      <Route exact path="/health" render={() => <Health />} />
    </Switch>
  </BrowserRouter>
);
