import { useEffect, useState } from "react";

import { apiClient } from "@app/query";

export const Health = () => {
  const [isSuccess, setIsSuccess] = useState(false);

  useEffect(() => {
    (async () => {
      await apiClient.get("/health");

      setIsSuccess(true);
    })();
  }, []);

  return <>{isSuccess ? "OK" : ""}</>;
};
