const fs = require('fs');
const path = require('path');

const prettierOptions = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../..', '.prettierrc'), 'utf8'));

module.exports = {
  extends: ['airbnb-typescript', 'prettier', 'plugin:react-hooks/recommended'],
  parserOptions: {
    project: './tsconfig.json',
  },
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': ['error', prettierOptions],
    'import/prefer-default-export': 'off',
    'react/react-in-jsx-scope': 'off',
    'react/jsx-key': ['error', { checkFragmentShorthand: true, checkKeyMustBeforeSpread: true }],
    'react/prop-types': 'off',
    '@typescript-eslint/no-shadow': 'off',
    'react/no-array-index-key': 'off',
    'no-nested-ternary': 'off',
    'no-param-reassign': [
      'error',
      {
        props: true,
        ignorePropertyModificationsForRegex: ['^state', '^draft', 'config', 'result', 'ctx', 'notification'],
      },
    ],
    'no-console': ['error', { allow: ['warn', 'error'] }],
    'react/jsx-props-no-spreading': 'off',
    'jsx-a11y/anchor-is-valid': 'off',
    'global-require': 'off',
    'jsx-a11y/interactive-supports-focus': 'off',
    'jsx-a11y/click-events-have-key-events': 'off',
    'jsx-a11y/control-has-associated-label': 'off',
    'react/no-this-in-sfc': 'off',
    'react/require-default-props': 'off',
  },
  overrides: [
    {
      files: ['**/*.ts?(x)'],
      rules: {
        'prettier/prettier': ['warn', prettierOptions],
        '@typescript-eslint/naming-convention': [
          'error',
          {
            selector: 'enum',
            format: ['UPPER_CASE'],
          },
        ],
      },
    },
  ],
  ignorePatterns: ['config', 'internals', 'scripts', 'cypress'],
};
