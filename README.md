## Available Scripts

You can run project in prod mode with command:
### `docker-compose up -d`


Run project with health check:
### `./health_check.sh`


Run project with export node_modules:
### `./export_modules.sh`


Run project in debug mode with live reloading
(copy file app.controller.ts to container, change it and see in browser
http://localhost:5555/health)
### `./debug_mode.sh`